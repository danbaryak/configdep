package dm.configdep.service.impl;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

import dm.configdep.service.TestService;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {

		manager.add(createComponent()
				.setInterface(TestService.class.getName(), null)
				.setImplementation(TestServiceImpl.class)
				.add(createConfigurationDependency().setPid("dm.configdep.service")));


	}

}
