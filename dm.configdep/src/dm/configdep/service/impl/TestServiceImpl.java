package dm.configdep.service.impl;

import dm.configdep.service.TestService;

public class TestServiceImpl implements TestService {

	public TestServiceImpl() {
		System.out.println("In TestServiceImpl constructor");
	}

	public void start() {
		System.out.println("started");
	}

	@Override
	public void doSomething() {
		System.out.println("Something");
	}

	public void destroy() {
		System.out.println("destroyed");
	}
}
