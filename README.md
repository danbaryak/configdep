# Amdatu Workspace Template

This project acts as a template for all other Amdatu projects in the sense that
it contains the minimum files to create a new workspace quickly.

## Principles and guidelines

1. The project should be based on Bndtools and Gradle. This allows for a
   uniform way of working and makes it easy for everybody to get familiar to
   new Amdatu projects;
2. Each workspace should be self-contained, that is, it should be possible for
   somebody to just check out a workspace and invoke a commandline build
   without any additional steps;
3. Amdatu projects should have at least a public source repository, a CI build,
   a public JIRA, a public Wiki/confluence and a public website. For the CI,
   JIRA and wiki, one can use the hosted Atlassian instance:
   [amdatu.atlassian.net](https://amdatu.atlassian.net). Source code is hosted
   at [Bitbucket](https://bitbucket.org/amdatu).


## Workspace layout

A typical Amdatu project workspace has the following contents:

* `README.md` - states the purpose and goals of the project, including some
  information on how to get started, and where to find additional information,
  such as links to the Amdatu website, source repository and so on;
* `LICENSE` - states the principal license of the project, currently, only
  [Apache License version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)
  is allowed;
* `NOTICE` - states the copyright notice and lists any required third-party
  notices in case third-party licenses are used that require such a notice;
* `build.gradle` - main entry point for Gradle;
* `gradle.properties` - properties used by Gradle and its plugins;
* `gradlew` - shell script to invoke the Gradle wrapper on Unix-like platforms;
* `gradlew.bat` - batch script to invoke the Gradle wrapper on Windows;
* `settings.gradle` - main settings for Gradle;
* `.gradle-wrapper/`
    * `gradle-wrapper.jar` - bootstrap code for Gradle wrapper;
    * `gradle-wrapper.properties` - bootstrap configuration for Gradle wrapper,
      including which version of Gradle is to be used;
* `cnf/` - Bnd(tools) configuration
    * `build.bnd` - provides the Bnd build settings, such as compiler versions
      and other workspace defaults;
    * ...

Note that the `gradle.properties` supplied in this template does **not** define
a default project name (`bnd_build` is empty), meaning that **all**
sub-projects in the workspace will be considered part of the project. In case
you want to build only a subset of sub-projects, set the `bnd_build` property
to the name of a sub-project and define its dependencies through a `-dependson`
clause in its `bnd.bnd` file.

### Settings

This template provides some defaults for the workspace settings:

- The Gradle version as used by the "gradle wrapper" is set to **2.1**, which
  is, as of September 2014, the latest stable release;
- Java source and target compiler is set to **1.7**. Projects are allowed to
  use other compiler settings, but if they do, they **must** document this
  **explicitly** in their project's README;
- For development with Eclipse, Bndtools 2.3 is *at least* required (though not
  enforced);
- The default release repository for Bnd/Bndtools is set to **Release**.

Additional settings might be defined or changed in the future, and **must** be
communicated through the Amdatu mailing lists to inform other users.


## License

All Amdatu projects are licensed under [Apache License, version
2.0](http://www.apache.org/licenses/LICENSE-2.0.html). For project
dependencies, it is preferred and strongly recommended for Amdatu projects to
use artifacts with a license compatible to the Apache License version 2.0, see
[this page on the Apache site](http://www.apache.org/legal/resolved.html#category-a). 

### Source file headers

Source files, that is, all files that make up a source release of an Amdatu
project, should be attributed with a proper license header, which must be set
to:

    /*
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

Note that *no* copyright header is present in this header. The Gradle build
script should be extended to perform automated checks on file headers on each
build.

### NOTICE file

The copyright notice should be placed in the individual `NOTICE` file inside
each Amdatu project. An example file (replace `${projectName}` and `${year}`
with their corresponding values):

    Amdatu ${projectName}
    Copyright (c) 2010-${year} - The Amdatu Foundation.
    
    This product includes software developed at
    The Amdatu Foundation (http://www.amdatu.org/).
    Licensed under the Apache License 2.0.


## Releases

Amdatu projects *must* release sources and *may* accompany a release with
pre-built binaries for convenience. The release procedure is documented on the
[Amdatu Confluence](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Release+Procedure).
To ease the whole release process, a [custom Gradle plugin](https://bitbucket.org/amdatu/amdatu-gradle-plugins) 
is available, that automates almost the entire release process. This template
is pre-configured to include the Amdatu release plugin.

Projects are allowed to retain a copy of the latest released binaries in their
Git repository (for convenience and/or baselining), and if so, **must** keep it
in the `Release` repository. Note that *Amdatu Repository* is **always** the
single source for **all** released project artifacts.

Note that the Amdatu release plugin does *not* modify bundle versions for you!
It assumes these versions are already correctly bumped according to the rules
of Semantic Versioning.

