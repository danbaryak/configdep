package dm.configdep.test;

import static org.junit.Assert.*;

import java.util.Hashtable;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import dm.configdep.service.TestService;

@RunWith(MockitoJUnitRunner.class)
public class ConfigDepTest {

	private final BundleContext context = FrameworkUtil.getBundle(ConfigDepTest.class).getBundleContext();

	@Before
	public void before() {
		// TODO add test setup here
	}

	@After
	public void after() {
		// TODO add test clear up here
	}

	@Test
	public void testServiceRemovedAfterConfigurationDeletion() throws Exception {

		CountDownLatch addedLatch = new CountDownLatch(1);
		CountDownLatch removedLatch = new CountDownLatch(1);
		BundleContext context = getBundleContext(TestService.class);
		ServiceTracker<?, ?> tracker = new ServiceTracker<>(context, TestService.class, new ServiceTrackerCustomizer<TestService, TestService>() {

			@Override
			public TestService addingService(ServiceReference<TestService> reference) {
				addedLatch.countDown();
				return context.getService(reference);
			}

			@Override
			public void modifiedService(ServiceReference<TestService> reference, TestService service) {
			}

			@Override
			public void removedService(ServiceReference<TestService> reference, TestService service) {
				removedLatch.countDown();
			}
		});

		tracker.open();

		ConfigurationAdmin configAdmin = context.getService(context.getServiceReference(ConfigurationAdmin.class));
		Configuration configuration = configAdmin.getConfiguration("dm.configdep.service", null);
		Hashtable<String, ?> props = new Hashtable<>();
		configuration.update(props);

		Object service = tracker.waitForService(5000);
		assertTrue(service instanceof TestService);

		configuration.delete();

		assertTrue(removedLatch.await(10, TimeUnit.SECONDS));

		configuration = configAdmin.getConfiguration("dm.configdep.service", null);
		configuration.update(new Hashtable<>());

		Object service2 = tracker.waitForService(5000);
		assertFalse("A new service instance should have been created", service == service2);

	}

	 public static BundleContext getBundleContext(Class<?> type) {
	        Bundle bundle = FrameworkUtil.getBundle(type);
	        Objects.requireNonNull(bundle, "No bundle available? Are we running in a OSGi test container?!");
	        return bundle.getBundleContext();
	 }

}